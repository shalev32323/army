import unittest
from pages.GooglePage import GooglePage
from tests.BaseTest import BaseTest
import HtmlTestRunner
import pytest
import allure


class GoogleTest(BaseTest):

    def test_search(self):
        gp = GooglePage(self.driver)
        gp.search_in_google("selenium")

    def test_validation(self):
        gp = GooglePage(self.driver)
        gp.check_results('Selenium')
        gp.click_on_first_result()


if __name__ == '__main__':
    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output='/Users/shalevzrihen/PycharmProjects/hsa/reports'))

