package com.company;
import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

public class Main {

    public static void main(String[] args) {
	// write your code here
        SelenideElement searchField =  $("[name='q']");

        open("https://www.google.co.il/?hl=iw");
        searchField.sendKeys("selenium");
        searchField.submit();
        $(".LC20lb.DKV0Md span",0).shouldHave(text("Selenium WebDriver"));
    }
}
