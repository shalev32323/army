from selenium import webdriver

driver = webdriver.Chrome("/Users/shalevzrihen/Documents/selenium/chromedriver")
try:
    driver.get('http://www.google.com/')
    search_field = driver.find_element_by_name('q')
    search_field.send_keys("selenium")
    search_field.submit()
    elements = driver.find_elements_by_css_selector(".LC20lb.DKV0Md span")
    assert elements[0].text == 'Selenium WebDriver'
finally:
    driver.close()


